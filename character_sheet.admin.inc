<?php
// $Id$
/**
 * @file
 * Adds administrative settings for character sheet, moved here to save on code footprint.
 */

/**
 * Implementation of admin settings form.
 */
function character_sheet_admin_settings($form, &$form_state) {
  $systems = module_invoke_all('character_sheet_register_systems');
  if (count($systems) > 0) {
    foreach ($systems as $key => $system) {
      $system_keys[$key] = $system['name'] . ' : ' . $system['description'];
    }
    $default_systems = variable_get('character_sheet_systems', array());
    $form['character_sheet_systems'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Enable game systems'),
      '#required' => TRUE,
      '#default_value' => $default_systems,
      '#options' => $system_keys,
      '#description' => t('Select the systems available to use with the character sheets.'),
    );
  }
  $form['character_sheet_statuses'] = array(
    '#type' => 'textarea',
    '#title' => t('Moderation states'),
    '#required' => TRUE,
    '#default_value' => variable_get('character_sheet_statuses', "in progress\nsubmitted for approval\napproved\nrejected"),
    '#description' => t('List moderation states for your character sheets. One per row.'),
  );
  $form['character_sheet_limited_statuses'] = array(
    '#type' => 'select',
    '#title' => t('Limited moderation states'),
    '#required' => TRUE,
    '#default_value' => variable_get('character_sheet_limited_statuses', array('in progress', 'submitted for approval')),
    '#options' => _character_sheet_allowed_statuses(TRUE),
    '#multiple' => TRUE,
    '#description' => t('Select moderation states for your character sheets that regular users are allowed to use.'),
  );
  $form['character_sheet_approved_statuses'] = array(
    '#type' => 'select',
    '#title' => t('Approved moderation states'),
    '#required' => TRUE,
    '#default_value' => variable_get('character_sheet_approved_statuses', array('approved')),
    '#options' => _character_sheet_allowed_statuses(TRUE),
    '#multiple' => TRUE,
    '#description' => t('Select moderation states for your character sheets that indicate the sheet is approved.'),
  );
  if(!empty($default_systems)) {
    foreach ($default_systems as $system) {
      if (function_exists($systems[$system]['admin_callback'])) {
        $additional_elements = call_user_func_array($systems[$system]['admin_callback'], array());
        if (!empty($additional_elements)) {
          $form[$system] = array(
            '#type' => 'fieldset',
            '#title' => $systems[$system]['name'],
            '#description' => $systems[$system]['description'],
            '#collapsible' => TRUE,
            '#collapsed' => TRUE,
          );
        }
        else {
          $form[$system] = array(
            '#title' => $systems[$system]['name'],
            '#description' => $systems[$system]['description'],
          );
        }
        foreach ($additional_elements as $key => $element) {
          $form[$system][$key] = $element;
        }
      }
    }
  }
  return system_settings_form($form);
}
