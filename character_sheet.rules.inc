<?php

/**
 * @file
 * Provides rules integration for character sheets.
 */

/**
 * Implements hook_rules_condition_info().
 */
function character_sheet_rules_condition_info() {
  $conditions = array();
  $conditions['character_sheet_condition_is_approved'] = array(
    'label' => t('Character sheet is approved'),
    'group' => t('Character sheet'),
    'parameter' => array(
      'sheet' => array(
        'type' => 'node',
        'label' => t('sheet'),
      ),
    ),
  );
  return $conditions;
}


/**
 * The callback for the condition 'character_sheet_condition_is_approved'.
 */
function character_sheet_condition_is_approved($sheet) {
  return _character_sheet_get_sheet_status($sheet->nid, $sheet->vid);
}

/**
 * Implements hook_rules_event_info().
 */
function character_sheet_rules_event_info() {
  $events = array();
  $events['character_sheet_event_updated'] = array(
    'label' => t('A character sheet is updated'),
    'group' => t('Character sheet'),
    'module' => 'character_sheet',
    'variables' => array(
      'sheet' => array(
        'type' => 'node',
        'label' => t('sheet'),
      ),
      'player' => array(
        'type' => 'user',
        'label' => t('player'),
      ),
    ),
  );
  $events['character_sheet_event_status_change'] = array(
    'label' => t('A character sheet status updated'),
    'group' => t('Character sheet status'),
    'module' => 'character_sheet',
    'variables' => array(
      'status' => array(
        'type' => 'text',
        'label' => t('status'),
      ),
      'sheet' => array(
        'type' => 'node',
        'label' => t('sheet'),
      ),
      'moderator' => array(
        'type' => 'user',
        'label' => t('moderator'),
      ),
    ),
  );
  $events['character_sheet_event_approved'] = array(
    'label' => t('A character sheet is approved'),
    'group' => t('Character sheet status'),
    'module' => 'character_sheet',
    'variables' => array(
      'status' => array(
        'type' => 'text',
        'label' => t('status'),
      ),
      'sheet' => array(
        'type' => 'node',
        'label' => t('sheet'),
      ),
      'moderator' => array(
        'type' => 'user',
        'label' => t('moderator'),
      ),
    ),
  );
  $events['character_sheet_event_disapproved'] = array(
    'label' => t('A character sheet is disapproved'),
    'group' => t('Character sheet status'),
    'module' => 'character_sheet',
    'variables' => array(
      'status' => array(
        'type' => 'text',
        'label' => t('status'),
      ),
      'sheet' => array(
        'type' => 'node',
        'label' => t('sheet'),
      ),
      'moderator' => array(
        'type' => 'user',
        'label' => t('moderator'),
      ),
    ),
  );
  return $events;
}